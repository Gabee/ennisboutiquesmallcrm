import './App.scss';
import Mainscreen from './components/mainscreen/Mainscreen';
import CreateAccount from './components/create_account/CreateAccount';
import Login from './components/login/Login'
import { BrowserRouter as Router, Switch, Route} from "react-router-dom";


function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/" component={Login}></Route>
        <Route path="/mainscreen" component={Mainscreen}></Route>
        <Route path="/create_account" component={CreateAccount}></Route>
      </Switch>
    </div>
  );
}

export default App;
