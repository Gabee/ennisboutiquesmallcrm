import React from "react";
import './Inventory.sass'

let Inventory = () => {
  return (
    <div id="container">
      searchbar
      <div id="search_add_bar">
        <div id="box1">
          <div id="contents_box">
          <input placeholder="Search for product"></input>
          <button class="custom_button">Add Product</button>
         </div>
        </div>
      </div>
      <div id="inventory_table">
          <div id="box2">
          </div>
      </div>
    </div>
  )
};

export default Inventory;
