const {app, BrowserWindow} = require('electron');

function createWindow(){
    win = new BrowserWindow({width: 1900, height: 1000});
    win.loadURL('http://localhost:3000');
}

app.on('ready', createWindow);