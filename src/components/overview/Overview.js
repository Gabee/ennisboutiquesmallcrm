import { React, useEffect, useState } from "react";
import "./Overview.scss";
import anime from 'animejs';
import {Switch, Route} from 'react-router-dom';

import search_icon from "../../images/MagnifyingGlass.png";
import bell_icon from "../../images/Bell.png";
import userprofile_image from "../../images/UserProfileImage.png";
import Inventory from "../inventory/Inventory.js";
import Dashboard from "../dashboard/Dashboard";

let Overview = () => {
  useEffect(() => {
    let iconFrame = document.querySelector(".NotificationIconFrame");
      iconFrame.addEventListener("mouseover", () => {
        if (notificationNumber > 0){
          anime({
            targets: '.HoverFrame',
            translateY: '99%',
          });
        }
      });

      iconFrame.addEventListener("mouseleave", () => {
        anime({
          targets: '.HoverFrame',
          translateY: '-37%',
        })
      })
  });

  const [searchBarFocus, setSearchBoxFocus] = useState(false);
  const [notificationNumber, setNotificationNumber] = useState(5);

  var currentDate = new Date().toDateString();

  let searchbox = document.getElementById("SearchBar");

  if (searchbox != null) {
    var inputElement = searchbox.querySelector("input");

    inputElement.addEventListener("focus", () => {
      setSearchBoxFocus(true);
    });
  }

  return (
    <div className="Overview">
      <div className="NavigationBar">
        <div className="SearchBarFrame">
          <div id="SearchBar">
            <input
              id="searchbox"
              type="text"
              placeholder="Search Bar Test"
              size="48"
            ></input>
            <img src={search_icon} alt="Search Icon" />
          </div>
        </div>
        <div className="NotificationFrame">
          <div className="DateFrame">
            <p>{currentDate}</p>
          </div>
          <div className="NotificationIconFrame">
            <img src={bell_icon} alt="Bell Icon" id="Bell" />
            {notificationNumber > 0 && (
              <div id="badge">
                <p>{notificationNumber}</p>
              </div>
            )}
            <div className="HoverFrame">
              {notificationNumber > 0 && (
                <p>You have stuff</p>
              )}
              {notificationNumber <= 0 && (
                <p>You have no alerts</p>
              )}
            </div>
          </div>
          <div className="ProfileFrame">
            <div id="UserName">User Name</div>
            <div id="ProfilePhoto">
              <img
                src={userprofile_image}
                alt="User Profile Image"
                id="UserProfileImage"
              />
            </div>
          </div>
        </div>
      </div>
      <div className="ContentBox">
                <Switch>
                  <Route exact path="/mainscreen/dashboard" component={Dashboard}></Route>
                  <Route path="/mainscreen/inventory" component={Inventory}></Route>
                  {/* <Route path="/login" component={Login}></Route> */}
                </Switch>
      </div>
    </div>
  );
};

export default Overview;
