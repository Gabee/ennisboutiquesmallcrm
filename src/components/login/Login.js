import React, { useEffect, useState } from "react";
import { Formik } from "formik";
import "./Login.scss";

import { Link } from 'react-router-dom'

import { ReactComponent as RockTheBoat } from "../../images/RockTheBoat.svg";

let Login = () => {
  return (
    <div className="Login">
      <div className="Left">
        <div className="MessageField">
          <span>
            New? Create an <Link to="/create_account">account</Link>
          </span>
        </div>
        <div className="LoginField">
          <h1>Login</h1>
          <div id="LoginFormField">
            <form id="LoginForm">
              <div className="FormField UsernameField">
                <h2>Email</h2>
                <div className="Bar">
                  <input></input>
                </div>
              </div>
              <div className="FormField EmailField">
                <h2>Password</h2>
                <div className="Bar">
                  <input type="password"></input>
                </div>
              </div>
              <div className="ButtonField">
                <Link to="/mainscreen/dashboard">
                <div className="CreateAccountButton">Login</div>
                </Link>
              </div>
            </form>
          </div>
        </div>
      </div>
      <div className="Right">
        <RockTheBoat></RockTheBoat>
      </div>
    </div>
  );
};

export default Login;
