import React from "react";
import "./Mainscreen.scss";

import Sidebar from "../sidebar/Sidebar";
import Overview from "../overview/Overview";

let Mainscreen = () => {
  return (
    <div className="Mainscreen">
        <Sidebar></Sidebar>
        <Overview></Overview>
    </div>
  );
};

export default Mainscreen;
