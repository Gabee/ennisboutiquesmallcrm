import React, { useEffect, useState } from "react";
import "./CreateAccount.scss";
import { ReactComponent as CatIllustrationSVG } from "../../images/CatIllustration.svg";
import { ReactComponent as CheckMarkSVG } from "../../images/CheckSquare.svg";
import { Formik } from "formik";
import { Link } from 'react-router-dom';

let CreateAccount = () => {
  let [confirmation, setConfirmation] = useState(false);

  useEffect(() => {
    let checkMark = document.querySelector(".CheckMark");
    if (confirmation) {
      checkMark.style.display = "";
    } else {
      checkMark.style.display = "none";
    }
  });

  let conf = () => {
    setConfirmation(!confirmation);
  };

  return (
    <div className="CreateAccount">
      <div className="Left">
        <CatIllustrationSVG></CatIllustrationSVG>
      </div>
      <div className="Right">
        <div className="LoginHeader">
          <span>
            Already have an account? <Link to="/">Login</Link>
          </span>
        </div>
        <div className="CreateAccountTitle">
          <h1> Create Account </h1>
        </div>
        <div className="FormContainer">
          <div className="InnerFormContainer">
            <Formik>
              <form>
                <div className="FormField UsernameField">
                  <h2>Username</h2>
                  <div className="Bar">
                    <input></input>
                  </div>
                </div>
                <div className="FormField EmailField">
                  <h2>Email</h2>
                  <div className="Bar">
                    <input type="email"></input>
                  </div>
                </div>
                <div className="PasswordFormField">
                  <div className="PasswordField">
                    <h2>Password</h2>
                    <div className="Bar" id="BarPass">
                      <input type="password"></input>
                    </div>
                  </div>
                  <div className="ConfirmationField">
                    <h2>Confirm</h2>
                    <div className="Bar" id="BarConf">
                      <input type="password"></input>
                    </div>
                  </div>
                </div>
                <div className="PrivacyPolicyField">
                  <div className="CheckBoxField">
                    <div className="CheckBox" onClick={conf}>
                      <CheckMarkSVG className="CheckMark"></CheckMarkSVG>
                    </div>
                  </div>
                  <div className="PolicyStatementField">
                    <p className="PolicyStatement">
                      By creating an account, you agree to the Terms of Service
                      and Conditions, and Privacy Policy
                    </p>
                  </div>
                </div>
                <div className="ButtonField">
                  <div className="CreateAccountButton">Create Account</div>
                </div>
              </form>
            </Formik>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CreateAccount;
