import React, { useEffect } from "react";
import "./Sidebar.scss";

import anime from "animejs";

import { Link } from "react-router-dom";
import { ReactComponent as LogoSVG } from "../../images/Frame 29.svg";
import table_icon from "../../images/Table.png";
import people_icon from "../../images/Users.png";
import power_icon from "../../images/power_icon.png";

let Sidebar = () => {
  let expanded = false;

  useEffect(() => {
    // Load navigation and grab div elements after componenets are loaded up.
    let nb = document.getElementsByClassName("NavigationBlock");
    let elem = document.createElement("div");
    for (let i = 0; i < 2; i++) {
      nb[i].addEventListener("mouseenter", (e) => {
        elem.setAttribute("id", "line-bar");
        nb[i].style.backgroundColor =
          "linear-gradient(267deg, rgba(255,255,255,1) 0%, rgba(87,123,249,0.7763480392156863) 100%)";
        nb[i].insertBefore(elem, nb[i].firstChild);
      });

      nb[i].addEventListener("mouseleave", (e) => {
        nb[i].style.backgroundColor = "";
      });
    }
  });

  let sidebarToggle = () => {
    let sidebar = document.querySelector(".Sidebar");

    if (!expanded) {
      anime({
        targets: ".Mainscreen",
        gridTemplateColumns: "8%",
      });

     
      sidebar.style.gridTemplateRows="15% 44% 41%";
      // sidebar.style.postion="relative"
      // sidebar.style.top="10%"
      sidebar.classList.add("collapse");

      // Hide navigation text.
      let nb = document.getElementsByClassName("NavigationBlock");
      for (let i = 0; i < 2; i++) {
        nb[i].getElementsByTagName("p")[0].style.display = "none";
      }

      expanded = !expanded;
    } else {
      anime({
        targets: ".Mainscreen",
        gridTemplateColumns: "14%",
      }
      );

      sidebar.style.gridTemplateRows="26% 43% 32%";
      // sidebar.style.postion=""
      // sidebar.style.top=""
      sidebar.classList.remove("collapse");

      // Display navigation text.
      let nb = document.getElementsByClassName("NavigationBlock");
      for (let i = 0; i < 2; i++) {
        nb[i].getElementsByTagName("p")[0].style.display = "";
      }

      expanded = !expanded;
    }
  };

  return (
    <div className="Sidebar">
      <div id="ProfileFrame">
        <div id="PhotoFrame" onClick={sidebarToggle}>
          <LogoSVG className="logoSVG"></LogoSVG>
        </div>
        <div id="RequestFrame">
          <div id="RequestButtonFrame">
            <p>Request For</p>
          </div>
        </div>
      </div>
      <div id="NavigationFrame">
         <div className="NavigationBlock" id="nav-block2">
          <div class="NavigationAlignment">
            <Link to="/mainscreen/dashboard">
              <img src={table_icon} alt="Table Icon" />
              <p>Dashboard</p>
            </Link>
          </div>
        </div>
        <div className="NavigationBlock" id="nav-block2">
          <div class="NavigationAlignment">
            <Link to="/mainscreen/inventory">
              <img src={people_icon} alt="People Icon" />
              <p>People</p>
            </Link>
          </div>
        </div>
      </div>
      <div id="SupportFrame">
        <Link to="/">
        <img src={power_icon} alt="Power Icon" />
        </Link>
      </div>
    </div>
  );
};

export default Sidebar;
