# Database Software Project

## UI/UX Preview
Note: One of the goals and inspiration from this is to use Figma to recreate Karol Kos CRM app for different use case purposes.

The technologies used for: (HTML/(SCSS/SASS)/Javscript)

Design Credit: [Karol Kos from widelab
](https://dribbble.com/shots/10871728-CRM-App)

### Products 
![Products Table](src/layout/Products&#32;Overlay.png)

### Adding Products
![Adding Product Modal](src/layout/Add&#32;Product&#32;Overlay.png)

### View/Edit Products
![View/Edit Product Modal](src/layout/Edit&#32;Product&#32;Overlay.png)

## Install
    npm install

## How to Run

This Project can run in two ways from web browser or electron.

**Warning**

The app is meant to be run on electron with a fix width and height (1400x900). Viewing will change depending on resolution if ranned outside of electron.

This app doesn't included responsive-design for mobile devices.

### Electron
    npm run electron-start

### Web
    npm run start

## Learn More

You can learn more in the [from my Gitlab Repo](https://gitlab.com/Gabee/ennisboutiquesmallcrm).
